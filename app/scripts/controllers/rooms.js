'use strict';

angular.module('vidconfApp')
  .controller('RoomsCtrl', function ($scope, $rootScope, $location, $window, syncData, chatService, roomService, moderatorService) {
    $scope.glued = true;
    $scope.open = true;
    var roomId = $location.path();
    var user = $rootScope.auth.user;
    console.log(roomId);
    var isModerator = moderatorService.isModerator(roomId);
    $scope.isModerator = isModerator;
    moderatorService.statusCheck(roomId);
    roomService.watch($scope, roomId);
    roomService.watchBan(roomId,user);
    roomService.watchModerator($rootScope,roomId,isModerator);
    roomService.setStatus(roomId,user);

    $scope.room = syncData($location.path());
    $scope.users = syncData($location.path() + '/users');

    $scope.msg = {};
    $scope.messages = chatService.getMessages($scope, roomId);
    $scope.addMessage = function(e) {
        if (e.keyCode !== 13){
          return;
        }
        chatService.addMessage($scope.auth.user.displayName, $scope.msg.body, roomId);
        $scope.msg.body = '';
      };
    $scope.banUser = function(user){
        moderatorService.banUser(roomId, user);
      };
    $scope.$on("$destroy", function(){
        roomService.clearRoom(roomId,user);
    });

  });
