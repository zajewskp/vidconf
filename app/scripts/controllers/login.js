'use strict';

angular.module('vidconfApp')
  .controller('LoginCtrl', function ($scope,simpleLogin, $rootScope) {
    $rootScope.auth = simpleLogin.init();
    //in login.jade
    $scope.login = function(service) {
      simpleLogin.login(service);
    };
    //in navbar.jade
    $scope.logout = function(){
      simpleLogin.logout();
    };
  });