'use strict';

/**
 * @ngdoc function
 * @name vidconfApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the vidconfApp
 */
angular.module('vidconfApp')
  .controller('MainCtrl', function ($scope, $location, syncData) {
    //room.jade
    Firebase.goOnline();
    $scope.component = 'info';
    $scope.setComponent = function(val){
      $scope.component =val;
    };
    $scope.rooms = syncData('rooms');
    $scope.statusClass = function(room){
      return room.status;
    };

    //roomForm.jade

    $scope.newRoom = {
        roomName: '',
        moderator: '',
        moderatorId: '',
        roomDetails: '',
        status: 'waiting',
        maxUsers: 3
      };
    $scope.valid = true;
    $scope.newRoom.submitTheForm = function() {
        $scope.newRoom.moderator = $scope.auth.user.displayName;
        $scope.newRoom.moderatorId = $scope.auth.user.uid;
        $scope.newRoom.maxUsers = parseInt($scope.newRoom.maxUsers, 10);
        $scope.rooms.$add($scope.newRoom).then(
          function(ref){
            console.log(ref.name());
            $location.path('rooms/' + ref.name());
          },
          function(error){
            console.log(error);
            $scope.valid = false;
          });
      };

    //roomInfo.jade
    $scope.open = true;
    $scope.selected = {
        roomName: '',
        moderator: '',
        roomDetails: '',
        maxUsers: null,
        status: '',
        id: '',
        users: null,
        usersIn: null
      };
    $scope.getInfo = function(room){
      $scope.selected = syncData('rooms/'+ room.$id);
      $scope.selected.id = room.$id;
    };
  });
