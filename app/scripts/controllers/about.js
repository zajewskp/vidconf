'use strict';

/**
 * @ngdoc function
 * @name vidconfApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the vidconfApp
 */
angular.module('vidconfApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
