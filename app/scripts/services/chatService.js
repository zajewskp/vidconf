'use strict';

angular.module('vidconfApp')
  .factory('chatService', function ($location, firebaseRef) {
    var messagesRef = firebaseRef('messages');
    var main = firebaseRef();
    return {
      getMessages: function ($scope, roomPath) {
        var roomId = roomPath.split('/')[2];
        var messages = [];
        main.child(roomPath +'/messages/').on('child_added', function(snap) {
          messagesRef.child(roomId + '/' + snap.name()).once('value', function(val){
            var applyFn = function(){
              if(val.val() !== null)
                messages.push(val.val());
            };
            $scope.$apply(applyFn);
          });
        });
        console.log(messages);
        return messages;
      },
      addMessage: function(from, body, roomPath) {
        var roomId = roomPath.split('/')[2];
        var message = messagesRef.child(roomId).push();
        message.set({from: from, body: body}, function() {
          var name = message.name();
          main.child(roomPath +'/messages/' + name).set(true);
        });
      }
    };
  });