'use strict';

angular.module('vidconfApp')
  .factory('loadRoom', function ($rootScope, firebaseRef) {
    var roomsRef = firebaseRef('rooms');
    var user = $rootScope.auth.user;
    return {
      load: function checkBan(roomName, callback) {
        var maxUsers = null;
        var roomRef = roomsRef.child(roomName);

        roomRef.child('maxUsers').once('value', function(snapshot){
          maxUsers = snapshot.val();
        });
        roomRef.child('banned/' + user.uid).once('value', function(snapshot){
          if(snapshot.val() !== null){
            return callback({status: 'You have been banned'});
          } else {
            addUser();
          }
        });
        function addUser(){
          roomRef.child('users/' + user.uid).once('value', function(snapshot){
            var exists = (snapshot.val() !== null);
            if(exists){
              return callback({status: 'waiting'});
            } else {
               postResponse();
            }
          });
        }
        function postResponse(){
          roomRef.child('status').once('value', function(snapshot){
            var error = (snapshot.val() === 'full');
            if(error) {
              return callback({status: 'full'});
            }
            else{
              roomRef.child('users/' + user.uid).set({username: user.displayName, userId: user.uid});
              return callback({status: 'waiting'});
            }
          });
        }
      }
    };
  });
