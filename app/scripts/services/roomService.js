'use strict';

angular.module('vidconfApp')
  .factory('roomService', function ($location, firebaseRef, $rootScope, $window) {
    var main = firebaseRef();

    return {
      watch: function ($scope, roomId) {
        $scope.$watch(function(){
          return $location.path();
        }, watchRes);
        function watchRes(value){
          if(value !== roomId){
            clearRoom(roomId);
          }
        }
      },
      clearRoom: function(roomId,user){
          var messages = new Firebase("https://vidconf.firebaseio.com/messages/" + roomId.split('/')[2]);
          main.child('/' + roomId +'/moderator').once('value', function(snapshot){
            if(snapshot.val() === $rootScope.auth.user.displayName){
              main.child('/' + roomId).remove();
              messages.remove();
            }
          });
          main.child('/' + roomId +'/users/' + user.uid).remove();
          Firebase.goOffline();
      },
      watchBan: function (roomId,user) {
        main.child('/' + roomId +'/banned/' + user.uid).on('value', function(snapshot){
          if(snapshot.val() === true){
            $window.alert('You have been kicked out by moderator');
            $location.path('/');
          }
        });
      },
      watchModerator: function ($rootScope,roomId,isModerator) {
        console.log("location path: "+ $location.path());
        console.log("room id: " + roomId);
        $rootScope.$on('$locationChangeStart', function(event, newUrl, oldUrl){
          if(newUrl === roomId){
                    console.log("WARUNEK SPEŁNIONY");
            main.child('/' + roomId + '/moderator').on('value', function(snapshot){
              if(snapshot.val() === null && isModerator === false){
                $window.alert('Moderator has left the room');
                $location.path('/');
              }
            });
          }
        });
      },
      setStatus: function (roomId,user) {
        var myUserRef = main.child('/' + roomId + '/users/' + user.uid);
        var connectedRef = new Firebase("https://vidconf.firebaseio.com//.info/connected");
        connectedRef.on("value", function(isOnline) {
          if (isOnline.val()) {
            myUserRef.onDisconnect().remove();
          }
        });

      }
    };
  });
