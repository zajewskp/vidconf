'use strict';

angular.module('vidconfApp')
  .factory('moderatorService', function ($rootScope, firebaseRef) {
      var main = firebaseRef();
      var mode = false;
      var maxUsers = null;
    $rootScope.$on('$locationChangeStart', function(event, newUrl, oldUrl){
        mode = false;
        maxUsers = null;
    });
      function init(roomId){
        main.child('/' + roomId + '/maxUsers').on('value', function(snapshot){
          maxUsers = snapshot.val();
        });
        main.child('/' + roomId + '/moderator').on('value', function(snapshot){
          if(snapshot.val() === $rootScope.auth.user.displayName){
            mode = true;
          }
        });
      }
    // Public API here
    //todo jak już sie raz było moderatorem to się jest w każdym kolejnym pokoju
    //todo2 uprzątnąć syf
    return {
      isModerator: function (roomId) {
        init(roomId);
        return mode;
      },
      banUser: function (roomId, user) {
        if(mode === true){
          main.child('/' + roomId + '/banned/' + user.$id).set(true);
          main.child('/' + roomId + '/users/' + user.$id).remove();
        }
      },
      statusCheck: function (roomId){
        if(mode === true){
          main.child('/' + roomId + '/usersIn').on('value', function(snapshot){
            if(snapshot.val() !== null){
              if(snapshot.val() === maxUsers){
                main.child('/' + roomId + '/status').set('full');
              } else{
                main.child('/' + roomId + '/status').set('waiting');
              }
            }
            else {
              main.child('/' + roomId + '/status').remove();
            }
          });
          main.child('/' + roomId + '/users').on('child_removed', function(oldChildSnapshot) {
              main.child('/' + roomId +'/usersIn').transaction(function(snapshot) {
                return snapshot-1;
              });
          });
          main.child('/' + roomId + '/users').on('child_added', function(oldChildSnapshot) {
              main.child('/' + roomId +'/usersIn').transaction(function(snapshot) {
                return snapshot+1;
              });
          });
        var myUserRef = main.child('/' + roomId + '/users/' + $rootScope.auth.user.uid);
        var connectedRef = new Firebase("https://vidconf.firebaseio.com//.info/connected");
        var messages = new Firebase("https://vidconf.firebaseio.com/messages/" + roomId.split('/')[2]);
        connectedRef.on("value", function(isOnline) {
          if (isOnline.val()) {
            main.child('/' + roomId +'/moderator').on('value', function(snapshot){
                if(snapshot.val() === $rootScope.auth.user.displayName){
                  main.child('/' + roomId).onDisconnect().remove();
                  messages.onDisconnect().remove();
                }
            });
          }
        });
      }
    }
    };
  });
