'use strict';

/**
 * @ngdoc overview
 * @name vidconfApp
 * @description
 * # vidconfApp
 *
 * Main module of the application.
 */
angular
  .module('vidconfApp', [
    'ngAnimate',
    'ngCookies',
    'firebase',
    'luegg.directives',
    'ui.bootstrap',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angularfire.firebase',
    'angularfire.login'
  ])
  .config(function ($routeProvider) {
      $routeProvider
        .when('/', {
          authRequired: true,
          templateUrl: 'views/main.html',
          controller: 'MainCtrl'
        })
        .when('/login', {
          authRequired: false,
          templateUrl: 'views/login.html',
          controller: 'LoginCtrl'
        })
        .when('/rooms/:id', {
          authRequired: true,
          templateUrl: 'views/rooms.html',
          controller: 'RoomsCtrl',
          resolve: {
            load:function($q, $route, loadRoom){
              var deferred = $q.defer();
              var roomName = $route.current.params.id;
              var successCb = function(result){
                if(angular.equals(result, {status: 'waiting'})){
                  deferred.resolve(result);
                }
                else if(angular.equals(result, {status: 'full'})){
                  deferred.reject('Room is full');
                }
                else{
                  console.log(result);
                  deferred.reject(result.status);
                }
              };

              loadRoom.load(roomName, successCb);
              return deferred.promise;
            }
          }
        })
        .otherwise({
          redirectTo: '/'
        });
    })

    .run(function ($rootScope, $location, $window) {
      $rootScope.$on('$routeChangeError', function (event, current, previous, rejection) {
        $window.alert('ROUTE CHANGE ERROR: ' + rejection);
        $location.path('/');
      });
    });
